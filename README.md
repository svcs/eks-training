# AWS Training - Running Containers on Amazon Elastic Kubernetes Service (Amazon EKS)

## Labs

* [Lab 1: Building an Amazon EKS Environment](lab1/)
* [Lab 2: Continuous deployment with GitOps](lab2/)
* [Lab 3: Log collection and analysis](lab3/)
* [Lab 4: Exploring Amazon EKS Communications](lab4/)
* [Lab 5: Securing Amazon EKS](lab5/)

## Labs files

* YAML files
* scripts
