# Lab 5: Securing Amazon EKS

## Additionat Resources

1. [Managing users or IAM roles for your cluster](https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html)
2. [Enable cluster logging with eksctl](https://eksctl.io/usage/cloudwatch-cluster-logging/)
3. [Logging Amazon EKS API calls with AWS CloudTrail](https://docs.aws.amazon.com/eks/latest/userguide/logging-using-cloudtrail.html)
4. [Project Calico](https://projectcalico.docs.tigera.io/about/about-calico)
5. [Get started with Kubernetes network policy](https://projectcalico.docs.tigera.io/security/kubernetes-network-policy)
6. [Creating OpenID Connect (OIDC) Identify Providers](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html)
7. [IAM Roles for Service Accounts](https://eksctl.io/usage/iamserviceaccounts/)
8. [AWS System Manager Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)
