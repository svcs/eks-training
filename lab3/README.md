# Lab 3: Log collection and analysis

## Additionat Resources

1. [What is Fluent Bit?](https://docs.fluentbit.io/manual/about/what-is-fluent-bit)
2. [AWS for Fluent Bit](https://docs.fluentbit.io/manual/installation/aws-container)
3. [Kubernetes DaemonSets](https://v1-19.docs.kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
4. [Partioning Data](https://docs.aws.amazon.com/athena/latest/ug/partitions.html)
5. [MSCK REPAIR TABLE](https://docs.aws.amazon.com/athena/latest/ug/msck-repair-table.html)
6. [SEtting UP Container Insights on Amazon EKS](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/deploy-container-insights-EKS.html)
7. [Using Container Insights](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ContainerInsights.html)
8. [AWS X-Ray Documentation](https://docs.aws.amazon.com/xray/index.html)
9. [AWS X-Ray for Kubernetes on GitHub](https://github.com/aws-samples/aws-xray-kubernetes)
10. [AWS X-Ray console](https://docs.aws.amazon.com/xray/latest/devguide/xray-console.html)
11. [AWS System Manager Session MAnager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)
