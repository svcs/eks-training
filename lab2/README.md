# Lab 2: Continuous deployment with GitOps

## Additionat Resources

1. [Weaveworks FLux](https://www.weave.works/oss/flux/)
2. [Docker](https://www.docker.com/why-docker)
3. [GitHub](https://github.com/features)
4. [AWS CodePipeline](https://docs.aws.amazon.com/codepipeline/index.html)
5. [Amazon Elastic Container Registry](https://aws.amazon.com/ecr/)
6. [Get Started with Flux](https://fluxcd.io/docs/get-started/)
7. [AWS System Manager Session MAnager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)
